

console.log("Hello World")

// S33 Activity:
// 1. In the S28 folder, create an activity folder and an index.html and a script.js file inside of it.
// 2. Link the index.js file to the index.html file.






// 3. Create a fetch request using the GET method that will retrieve all the to do list items from JSON Placeholder API.

// fetch("https://jsonplaceholder.typicode.com/todos")
// .then(res => res.json()) 
// .then(response => console.log(response)) 

fetch("https://jsonplaceholder.typicode.com/todos",
    {
        method: "GET"
    })
    .then(response => response.json())
    .then(json => console.log(json))



// 4. Using the data retrieved, create an array using the map method to return just the title of every item and print the result in the console.


fetch("https://jsonplaceholder.typicode.com/todos",
    {
        method: "GET",
        headers: {
            'Content-Type': 'application/json'
        },
    })
    .then(response => response.json())
    .then((response) => {

        let responseTitle = response.map((todo => {
            return todo.title;
        }))
    
        console.log(responseTitle);
    
    });
    



// 5. Create a fetch request using the GET method that will retrieve a single to do list item from JSON Placeholder API.


fetch("https://jsonplaceholder.typicode.com/todos/1",
    {
        method: "GET",
        headers: {
            'Content-Type': 'application/json'
        }
    })
    .then(response => response.json())
    .then(json => console.log(json))



// 6. Using the data retrieved, print a message in the console that will provide the title and status of the to do list item.


fetch("https://jsonplaceholder.typicode.com/todos/1",
    {
        method: "GET",
        headers: {
            'Content-Type': 'application/json'
        }
    })
    // .then((res) => console.log(res.status))
    .then((res) => {
        
        console.log(res.status)
        return res.json()
            
    })
    .then((res) => console.log(res.title)) 




// 7. Create a fetch request using the POST method that will create a to do list item using the JSON Placeholder API.



fetch("https://jsonplaceholder.typicode.com/todos",
    {
        method: "POST",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            title: "Created To Do List Item",
            userId: 1,
            completed: false

        })
    })
    .then(response => response.json())
    .then(json => console.log(json))


// 8. Create a fetch request using the PUT method that will update a to do list item using the JSON Placeholder API.


fetch("https://jsonplaceholder.typicode.com/todos/1",
    {
        method: "PUT",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
   
            title: "Updated To Do List Item",
            userId: 1

        })
    })
    .then(response => response.json())
    .then(json => console.log(json))



// 9. Update a to do list item by changing the data structure to contain the following properties:
// - Title
// - Description
// - Status
// - Date Completed
// - User ID


fetch("https://jsonplaceholder.typicode.com/todos/1",
    {
        method: "PATCH",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            description: "To update my to do list with a different data structure",
            status: "Pending",
            title: "Updated To Do List Item",
            userId: 1,
            dateCompleted: "Pending"


        })
    })
    .then(response => response.json())
    .then(json => console.log(json))



    // 10. Create a fetch request using the PATCH method that will update a to do list item using the JSON Placeholder API.


    fetch("https://jsonplaceholder.typicode.com/todos/1",
    {
        method: "PATCH",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            description: "To update the my to do list with a different data structure",
            status: "Pending",
            title: "Updated To Do List Item",
            userId: 1,
            dateCompleted: "Pending",
            completed: false
 


        })
    })
    .then(response => response.json())
    .then(json => console.log(json))


// 11. Update a to do list item by changing the status to complete and add a date when the status was changed.

    fetch("https://jsonplaceholder.typicode.com/todos/1",
    {
        method: "PATCH",
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({

            status: "Complete",
            dateCompleted: "07/09/21"
 


        })
    })
    .then(response => response.json())
    .then(json => console.log(json))



// 12. Create a fetch request using the DELETE method that will delete an item using the JSON Placeholder API.


fetch("https://jsonplaceholder.typicode.com/todos/1",
{
    method: "DELETE",

})
.then(response => response.json())
.then(json => console.log(json))